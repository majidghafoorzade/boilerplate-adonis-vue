'use strict'
const User = use('App/Models/User');
const { validate } = use('Validator');


class AuthController {

    async register ({ request, response }) {

        const rules = {
            email: 'required|email|unique:users,email',
            password: 'required|min:6|max:24',
            username: 'required|min:3|max:32|alpha_numeric|unique:users,username'
        }

        const all = request.all();
        const validation = await validate(all, rules);

        if(!validation.fails()){
            const user = new User()
            user.username = all.username;
            user.email = all.email;
            user.password = all.password;
            user.role = 'user';
            user.status = 'registered';
            await user.save();
            var dbResult = true;
        } else {
            var dbResult = validation.messages();
        }

        
        return {result: dbResult}
    }
}

module.exports = AuthController
