import Vue from 'vue'
import Router from 'vue-router'

import Index from '@/components/Index'
import About from '@/components/About'
import Login from '@/components/Login'
import PageNotFound from '@/components/PageNotFound'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'Index',
            component: Index
        },
        {
            path: '/about',
            name: 'About',
            component: About
        },
        {
            path: '/login',
            name: 'Login',
            component: Login
        },
        {
            path: "*",
            component: PageNotFound
        }
    ]
})