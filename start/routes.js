'use strict'

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')


Route.post("/auth/register", "AuthController.register").prefix("api");
Route.post("*", () => {
    return { result: false };
}).prefix("api");


Route.any('*', ({view}) =>  view.render('app'))
